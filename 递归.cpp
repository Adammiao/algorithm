/**
 * 递归算法
 * - 递归是什么？
 * - 递归是一种调用自身的编程思想。
 * 下面是一个求阶乘的实例。
 */

//题目：定义一个函数jiecheng，使其返回参数的阶乘。
//样例输入: 5
//样例输出: 120

#include <iostream>                     //用于输入/输出
using namespace std;                    //省去std::

long jiecheng(long arg) {               //程序核心
    if(arg == 1) {                      //递归停止的地方
        return 1;
    }
    return arg * jiecheng(arg - 1);     //返回结果，这是一个递归的过程。
}

int main( void ) {                      //C++程序入口， void可省略
    long n = 0;                          //n的阶乘，输入n
    cin >> n;
    cout << jiecheng(n);                //调用函数并输出
    return 0;
}